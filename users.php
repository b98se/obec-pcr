<?php 
session_start();
include("config.php");
include("function.php");
// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area'))) die();


include("head.php");
include("connect.php");

if($_SESSION['u_type'] == 'area') $db->where("area_id", $_SESSION['smis']);
$users = $db->get('view_user_school'); 
?>

<section class="content-header">
<h1> รายชื่อผู้ใช้ </h1>
</section> <!-- content-header -->

<section class="content">

<div class="box">
<div class="box-body">

<table id="example1" class="table table-bordered table-hover">
<thead>
<tr>
  <th>ที่</th>
  <th>username</th>
  <th>password</th>
  <th>ชื่อ</th>
  <th>หน่วยงาน</th>
  <th>ประเภทผู้ใช้</th>
</tr>
</thead>
<tbody>
<?php
foreach($users as $i => $u){
	echo "<tr>
			  <td>" .($i+1). "</td>
			  <td>{$u['usr']}</td>
			  <td>{$u['pwd']}</td>
			  <td>{$u['user_name']}</td>
			  <td>{$u['school_name']}</td>
			  <td>{$u_type_list[$u['u_type']]}</td>
			</tr>";
}
?>

</table>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->


<script>
  $(function () {
    $('#example1').DataTable({"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]})
  })
</script>

<?php include("foot.php") ?>