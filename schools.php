<?php 
session_start();
include("config.php");
include("function.php");
include("head.php");
include("connect.php");

$db->orderBy("sch_order","asc");
$schools = $db->get('school'); //contains an Array of all users 
?>

<section class="content-header">
<h1> รายชื่อโรงเรียน </h1>
</section> <!-- content-header -->

<section class="content">

<div class="box">
<div class="box-body">

<table id="example1" class="table table-bordered table-hover">
<thead>
<tr>
  <th>id</th>
  <th>รหัส smis</th>
  <th>ชื่อ</th>
  <th>อำเภอ</th>
  <th>จังหวัด</th>
</tr>
</thead>
<tbody>
<?php
foreach($schools as $u){
	echo "<tr>
			  <td>{$u['sch_order']}</td>
			  <td>{$u['smis']}</td>
			  <td>{$u['name']}</td>
			  <td>{$u['amp_name']}</td>
			  <td>{$prov_list[$u['prov_id']]}</td>
			</tr>";
}
?>

</table>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->


<script>
  $(function () {
    $('#example1').DataTable({"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]})
  })
</script>

<?php include("foot.php") ?>