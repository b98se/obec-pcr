<?php
function mysql2thaidate($date, $format='short' , $num= false){
	global $month;
	
	$date = date_create_from_format('Y-m-d', $date);
    if(!$date){
        return "";
        exit();
    }
	$yyyy = date_format($date, 'Y');
    $Y['long'] = $yyyy + 543;
    $Y['short'] = substr($Y['long'], -2);
    $m = date_format($date, 'm');
    $d = date_format($date, 'd');

    if ($num == true)
        return $d.'/'.$m.'/'.$Y[$format];
    else
        return $d.' '.$month[$format][$m].' '.$Y[$format];
}

function thaidate2mysql($date){

    list($d,$m,$Y) = explode('/',$date);
	$d = str_pad($d,2,"0",STR_PAD_LEFT);
	$m = str_pad($m,2,"0",STR_PAD_LEFT);

    return $Y.$m.$d;
}

function authorize($uGroup, $rejectMsg = 'คุณไม่มีสิทธิ์ ใช้งานหน้านี้', $rejectUrl = 'index.php'){
	if(!chkAuthorize($uGroup) ){
		jsAlert($rejectMsg);
		goUrl($rejectUrl);
		return false;
	}
	return true;
}

function chkAuthorize($uGroup){
	if(in_array($_SESSION['u_type'], $uGroup) ){
		return true;
	}else{
		return false;
	}
}

function goUrl($url){
	echo " <meta http-equiv='refresh' content='0;url={$url}'> ";
}
function goBack(){
	echo " <script>window.history.go(-1);</script> ";
}
function jsAlert($msg){
	 echo "<meta charset='utf-8'>";
	echo " <script>alert(\"{$msg}\");</script> "; 
 }

function fileUpload($file, $fileName){
	global $target_dir, $allowFileType;
/*	
	if(!file_exists($target_dir.date("Ym"))){
		mkdir($target_dir.date("Ym"), 0777,true);
		chmod($target_dir.date("Ym"),0777);
	}
*/
	$target_file = $target_dir . basename($file["name"]);
	$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	$target_file_name = $target_dir . $fileName .'.'. $fileType;

	// Allow certain file formats
	$d = array();
	if(!in_array($fileType, $allowFileType)){
		$d['status'] = false;
		$d['error'] = 'ไม่รองรับไฟล์ชนิดนี้';
		return $d;
	}

	if (move_uploaded_file($file["tmp_name"], $target_file_name)) {
		$d['fileUploaded'] = $fileName .'.'. $fileType;
		$d['status'] = true;
		
	} else {
		$d['status'] = false;
		$d['error'] = "Sorry, there was an error uploading your file.";
	}
	return $d;
}

?>