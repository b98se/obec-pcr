/*
 Navicat Premium Data Transfer

 Source Server         : 0 yala1_pracharath
 Source Server Type    : MySQL
 Source Server Version : 100121
 Source Host           : www.yala1.go.th:3306
 Source Schema         : yala1_pracharath

 Target Server Type    : MySQL
 Target Server Version : 100121
 File Encoding         : 65001

 Date: 29/12/2019 01:12:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pcr_form
-- ----------------------------
DROP TABLE IF EXISTS `pcr_form`;
CREATE TABLE `pcr_form`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` varchar(13) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bdate` date NULL DEFAULT NULL,
  `religion` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `stud_level` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `smis` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `consider` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file1_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file2_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `form_date` date NULL DEFAULT NULL,
  `create_user_id` int(11) NULL DEFAULT NULL,
  `update_user_id` int(11) NULL DEFAULT NULL,
  `create_at` datetime(0) NULL DEFAULT NULL,
  `update_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_pid`(`pid`) USING BTREE,
  INDEX `idx_smis`(`smis`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pcr_form
-- ----------------------------
INSERT INTO `pcr_form` VALUES (1, '1949800258272', 'm1', 'มนตรี3', 'มะสมัน3', '2000-03-03', 'i', 'a1', '90032001', 'insurgency', '1_1.pdf', 'หนังสือจังหวัด.pdf', '32_2.png', 'b2.png', '2020-03-03', NULL, 63, NULL, '2019-12-29 01:03:44');
INSERT INTO `pcr_form` VALUES (22, '1949800258273', 'm1', 'มนตรี1', 'มะสมัน1', '1998-02-01', 'b', 'a1', '90032001', 'income', NULL, 'หนังสือจังหวัด.pdf', '32_2.png', 'b2.png', '2020-01-01', NULL, 63, '2019-12-27 11:09:01', '2019-12-29 01:03:44');
INSERT INTO `pcr_form` VALUES (30, '1949800258274', 'm1', 'มนตรี2', 'มะสมัน2', '1999-02-02', 'b', 'a2', '90032001', 'insurgency', NULL, 'หนังสือจังหวัด.pdf', '32_2.png', 'b2.png', '2020-01-02', NULL, 63, '2019-12-27 11:17:35', '2019-12-29 01:03:44');
INSERT INTO `pcr_form` VALUES (32, '4444444444444', 'f2', 'น้องสี่', 'แซ่สี่', '2001-04-04', 'e', 'p4', '90032012', 'income', '32_1.pdf', '25581215_164144_3393.pdf', '32_2.png', 'b2.png', '2020-01-04', 63, 63, '2019-12-29 01:01:53', '2019-12-29 01:03:44');

-- ----------------------------
-- Table structure for pcr_school
-- ----------------------------
DROP TABLE IF EXISTS `pcr_school`;
CREATE TABLE `pcr_school`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `smis` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `area_id` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `amp_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prov_id` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sch_order` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_smis`(`smis`) USING BTREE,
  INDEX `idx_area_id`(`area_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pcr_school
-- ----------------------------
INSERT INTO `pcr_school` VALUES (1, '96010011', '9601', 'บ้านเขาตันหยงมิตรภาพที่ 153', 'เมือง', '96', 1);
INSERT INTO `pcr_school` VALUES (2, '96010079', '9601', 'บ้านยี่งอ', 'ยี่งอ', '96', 2);
INSERT INTO `pcr_school` VALUES (3, '96010052', '9601', 'บ้านบูเกะสูดอ', 'บาเจาะ', '96', 3);
INSERT INTO `pcr_school` VALUES (4, '96010150', '9601', 'บ้านยะบะ (อุปการวิทยา)', 'รือเสาะ', '96', 4);
INSERT INTO `pcr_school` VALUES (5, '96010183', '9601', 'ชุมชนบ้านซากอ', 'ศรีสาคร', '96', 5);
INSERT INTO `pcr_school` VALUES (6, '96020084', '9602', 'บ้านมูโนะ', 'สุไหงโก-ลก', '96', 6);
INSERT INTO `pcr_school` VALUES (7, '96020067', '9602', 'นิคมพัฒนา 10', 'สุคิริน', '96', 7);
INSERT INTO `pcr_school` VALUES (8, '96020107', '9602', 'บ้านบาโงกือเต', 'สุไหงปาดี', '96', 8);
INSERT INTO `pcr_school` VALUES (9, '96020010', '9602', 'บ้านตาบา', 'ตากใบ', '96', 9);
INSERT INTO `pcr_school` VALUES (10, '96010096', '9603', 'บ้านกาเด็ง', 'ระแงะ', '96', 10);
INSERT INTO `pcr_school` VALUES (11, '96020149', '9603', 'บ้านบูเก๊ะตาโมงมิตรภาพที่ 128', 'เจาะไอร้อง', '96', 11);
INSERT INTO `pcr_school` VALUES (12, '96020130', '9603', 'บ้านไอร์โซ', 'จะแนะ', '96', 12);
INSERT INTO `pcr_school` VALUES (13, '94010008', '9401', 'ชุมชนบ้านกรือเซะ', 'เมือง', '94', 13);
INSERT INTO `pcr_school` VALUES (14, '94010101', '9401', 'บ้านมะปริง', 'ยะหริ่ง', '94', 14);
INSERT INTO `pcr_school` VALUES (15, '94020053', '9401', 'วัดมุจลินทวาปีวิหาร (เพชรานุกูลกิจ)', 'หนองจิก', '94', 15);
INSERT INTO `pcr_school` VALUES (16, '94010040', '9401', 'บ้านบางมะรวด', 'ปะนาเระ', '94', 16);
INSERT INTO `pcr_school` VALUES (17, '94020013', '9402', 'นิคมสร้างตนเองโคกโพธิ์ มิตรภาพที่ 148', 'โคกโพธิ์', '94', 17);
INSERT INTO `pcr_school` VALUES (18, '94020154', '9402', 'ต้นพิกุล', 'ยะรัง', '94', 18);
INSERT INTO `pcr_school` VALUES (19, '94020107', '9402', 'บ้านน้ำใส', 'มายอ', '94', 19);
INSERT INTO `pcr_school` VALUES (20, '94020164', '9402', 'บ้านวังกว้าง', 'แม่ลาน', '94', 20);
INSERT INTO `pcr_school` VALUES (21, '94010093', '9403', 'บ้านตะโละไกรทอง', 'ไม้แก่น', '94', 21);
INSERT INTO `pcr_school` VALUES (22, '94010088', '9403', 'บ้านช่องแมว', 'สายบุรี', '94', 22);
INSERT INTO `pcr_school` VALUES (23, '94020121', '9403', 'บ้านมะนังยง', 'ทุ่งยางแดง', '94', 23);
INSERT INTO `pcr_school` VALUES (24, '94010152', '9403', 'บ้านโลทู', 'กะพ้อ', '94', 24);
INSERT INTO `pcr_school` VALUES (25, '95010020', '9501', 'บ้านพร่อน', 'เมือง', '95', 25);
INSERT INTO `pcr_school` VALUES (26, '95010071', '9501', 'บ้านตะโละหะลอ', 'รามัน', '95', 26);
INSERT INTO `pcr_school` VALUES (27, '95010109', '9501', 'บ้านสะเอะ', 'กรงปีนัง', '95', 27);
INSERT INTO `pcr_school` VALUES (28, '95020042', '9502', 'บันนังสตาอินทรฉัตรมิตรภาพที่ 200 ที่ระลึก ส.ร.อ.', 'บันนังสตา', '95', 28);
INSERT INTO `pcr_school` VALUES (29, '95020072', '9502', 'บ้านบาโงยซิแน', 'ยะหา', '95', 29);
INSERT INTO `pcr_school` VALUES (30, '95020094', '9502', 'บ้านบันนังดามา', 'กาบัง', '95', 30);
INSERT INTO `pcr_school` VALUES (31, '95020064', '9503', 'บ้านเยาะ', 'ธารโต', '95', 31);
INSERT INTO `pcr_school` VALUES (32, '90030092', '9003', 'บ้านกระอาน', 'เทพา', '90', 32);
INSERT INTO `pcr_school` VALUES (33, '90030152', '9003', 'บ้านบาโหย', 'สะบ้าย้อย', '90', 33);
INSERT INTO `pcr_school` VALUES (34, '90030081', '9003', 'บ้านสะพานเคียน', 'นาทวี', '90', 34);
INSERT INTO `pcr_school` VALUES (35, '90030023', '9003', 'กระจายสุทธิธรรมโมอนุสรณ์', 'จะนะ', '90', 35);
INSERT INTO `pcr_school` VALUES (36, '96022008', '101715', 'สวนพระยาวิทยา', 'จะแนะ', '96', 36);
INSERT INTO `pcr_school` VALUES (37, '96022010', '101715', 'บูกิตประชาอุปถัมภ์', 'เจาะไอร้อง', '96', 37);
INSERT INTO `pcr_school` VALUES (38, '96012004', '101715', 'บาเจาะ', 'บาเจาะ', '96', 38);
INSERT INTO `pcr_school` VALUES (39, '96012005', '101715', 'ร่มเกล้า', 'ยี่งอ', '96', 39);
INSERT INTO `pcr_school` VALUES (40, '96012006', '101715', 'ตันหยงมัส', 'ระแงะ', '96', 40);
INSERT INTO `pcr_school` VALUES (41, '96012008', '101715', 'เรียงราษฎ์อุปถัมภ์', 'รือเสาะ', '96', 41);
INSERT INTO `pcr_school` VALUES (42, '96022002', '101715', 'เวียงสุวรรณวิทยาคม', 'แว้ง', '96', 42);
INSERT INTO `pcr_school` VALUES (43, '96012009', '101715', 'ศรีวารินทร์', 'ศรีสาคร', '96', 43);
INSERT INTO `pcr_school` VALUES (44, '96022004', '101715', 'สุคิรินวิทยา', 'สุคิริน', '96', 44);
INSERT INTO `pcr_school` VALUES (45, '96022006', '101715', 'ธัญธารวิทยา', 'สุไหงปาดี', '96', 45);
INSERT INTO `pcr_school` VALUES (46, '94012008', '101715', 'วังกะพ้อพิทยาคม', 'กะพ้อ', '94', 46);
INSERT INTO `pcr_school` VALUES (47, '94022006', '101715', 'ทุ่งยางแดงพิทยาคม', 'ทุ่งยางแดง', '94', 47);
INSERT INTO `pcr_school` VALUES (48, '94012004', '101715', 'วุฒิชัยวิทยา', 'ปานาเระ', '94', 48);
INSERT INTO `pcr_school` VALUES (49, '94022005', '101715', 'ศิริราษฏร์สามัคคี', 'มายอ', '94', 49);
INSERT INTO `pcr_school` VALUES (50, '94012005', '101715', 'สายบุรี \"แจ้งประคาร\"', 'สายบุรี', '94', 50);
INSERT INTO `pcr_school` VALUES (51, '94022009', '101715', 'แม่ลานวิทยา', 'แม่ลาน', '94', 51);
INSERT INTO `pcr_school` VALUES (52, '94012006', '101715', 'ไม้แก่นกิตติวิทย์', 'ไม้แก่น', '94', 52);
INSERT INTO `pcr_school` VALUES (53, '94022007', '101715', 'ประตูโพธิ์วิทยา', 'ยะรัง', '94', 53);
INSERT INTO `pcr_school` VALUES (54, '94022004', '101715', 'ยาบีบรรณวิทย์', 'หนองจิก', '94', 54);
INSERT INTO `pcr_school` VALUES (55, '95022006', '101715', 'กาบังพิทยาคม', 'กาบัง', '95', 55);
INSERT INTO `pcr_school` VALUES (56, '95022004', '101715', 'ธารโตวัฑฒวิทย์', 'ธารโต', '95', 56);
INSERT INTO `pcr_school` VALUES (57, '95022003', '101715', 'บันนังสตาวิทยา', 'บันนังสตา', '95', 57);
INSERT INTO `pcr_school` VALUES (58, '95022002', '101715', 'จันทร์ประภัสสร์อนุสรณ์', 'เบตง', '95', 58);
INSERT INTO `pcr_school` VALUES (59, '95012002', '101715', 'รามันห์ศิริวิทย์', 'รามัน', '95', 59);
INSERT INTO `pcr_school` VALUES (60, '95022005', '101715', 'ยะหาศิรยานุกูล', 'ยะหา', '95', 60);
INSERT INTO `pcr_school` VALUES (61, '90032004', '101716', 'จะนะชนูปถัมภ์', 'จะนะ', '90', 61);
INSERT INTO `pcr_school` VALUES (62, '90032005', '101716', 'ทับช้างวิทยาคม', 'นาทวี', '90', 62);
INSERT INTO `pcr_school` VALUES (63, '90032012', '101716', 'สะบ้าย้อยวิทยา', 'สะบ้าย้อย', '90', 63);
INSERT INTO `pcr_school` VALUES (64, '90032001', '101716', 'เทพา', 'เทพา', '90', 64);

-- ----------------------------
-- Table structure for pcr_user
-- ----------------------------
DROP TABLE IF EXISTS `pcr_user`;
CREATE TABLE `pcr_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pwd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `smis` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_usr`(`usr`) USING BTREE,
  INDEX `idx_smis`(`smis`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pcr_user
-- ----------------------------
INSERT INTO `pcr_user` VALUES (1, '96010011', '96010011', 'บ้านเขาตันหยง', '96010011', 'sch');
INSERT INTO `pcr_user` VALUES (2, '96010079', '96010079', 'บ้านยี่งอ', '96010079', 'sch');
INSERT INTO `pcr_user` VALUES (3, '96010052', '96010052', 'บ้านบูเกะสูดอ', '96010052', 'sch');
INSERT INTO `pcr_user` VALUES (4, '96010150', '96010150', 'บ้านยะบะ', '96010150', 'sch');
INSERT INTO `pcr_user` VALUES (5, '96010183', '96010183', 'ชุมชนบ้านซากอ', '96010183', 'sch');
INSERT INTO `pcr_user` VALUES (6, '96020084', '96020084', 'บ้านมูโนะ', '96020084', 'sch');
INSERT INTO `pcr_user` VALUES (7, '96020067', '96020067', 'นิคมพัฒนา 10', '96020067', 'sch');
INSERT INTO `pcr_user` VALUES (8, '96020107', '96020107', 'บ้านบาโงกือเต', '96020107', 'sch');
INSERT INTO `pcr_user` VALUES (9, '96020010', '96020010', 'บ้านตาบา', '96020010', 'sch');
INSERT INTO `pcr_user` VALUES (10, '96010096', '96010096', 'บ้านกาเด็ง', '96010096', 'sch');
INSERT INTO `pcr_user` VALUES (11, '96020149', '96020149', 'บ้านบูเก๊ะตาโมง', '96020149', 'sch');
INSERT INTO `pcr_user` VALUES (12, '96020130', '96020130', 'บ้านไอร์โซ', '96020130', 'sch');
INSERT INTO `pcr_user` VALUES (13, '94010008', '94010008', 'ชุมชนบ้านกรือเซะ', '94010008', 'sch');
INSERT INTO `pcr_user` VALUES (14, '94010101', '94010101', 'บ้านมะปริง', '94010101', 'sch');
INSERT INTO `pcr_user` VALUES (15, '94020053', '94020053', 'วัดมุจลินทวาปีวิหาร', '94020053', 'sch');
INSERT INTO `pcr_user` VALUES (16, '94010040', '94010040', 'บ้านบางมะรวด', '94010040', 'sch');
INSERT INTO `pcr_user` VALUES (17, '94020013', '94020013', 'นิคมสร้างตนเองโคกโพธิ์', '94020013', 'sch');
INSERT INTO `pcr_user` VALUES (18, '94020154', '94020154', 'ต้นพิกุล', '94020154', 'sch');
INSERT INTO `pcr_user` VALUES (19, '94020107', '94020107', 'บ้านน้ำใส', '94020107', 'sch');
INSERT INTO `pcr_user` VALUES (20, '94020164', '94020164', 'บ้านวังกว้าง', '94020164', 'sch');
INSERT INTO `pcr_user` VALUES (21, '94010093', '94010093', 'บ้านตะโละไกรทอง', '94010093', 'sch');
INSERT INTO `pcr_user` VALUES (22, '94010088', '94010088', 'บ้านช่องแมว', '94010088', 'sch');
INSERT INTO `pcr_user` VALUES (23, '94020121', '94020121', 'บ้านมะนังยง', '94020121', 'sch');
INSERT INTO `pcr_user` VALUES (24, '94010152', '94010152', 'บ้านโลทู', '94010152', 'sch');
INSERT INTO `pcr_user` VALUES (25, '95010020', '95010020', 'บ้านพร่อน', '95010020', 'sch');
INSERT INTO `pcr_user` VALUES (26, '95010071', '95010071', 'บ้านตะโละหะลอ', '95010071', 'sch');
INSERT INTO `pcr_user` VALUES (27, '95010109', '95010109', 'บ้านสะเอะ', '95010109', 'sch');
INSERT INTO `pcr_user` VALUES (28, '95020042', '95020042', 'บันนังสตาอินทรฉัตร', '95020042', 'sch');
INSERT INTO `pcr_user` VALUES (29, '95020072', '95020072', 'บ้านบาโงยซิแน', '95020072', 'sch');
INSERT INTO `pcr_user` VALUES (30, '95020094', '95020094', 'บ้านบันนังดามา', '95020094', 'sch');
INSERT INTO `pcr_user` VALUES (31, '95020064', '95020064', 'บ้านเยาะ', '95020064', 'sch');
INSERT INTO `pcr_user` VALUES (32, '90030092', '90030092', 'บ้านกระอาน', '90030092', 'sch');
INSERT INTO `pcr_user` VALUES (33, '90030152', '90030152', 'บ้านบาโหย', '90030152', 'sch');
INSERT INTO `pcr_user` VALUES (34, '90030081', '90030081', 'บ้านสะพานเคียน', '90030081', 'sch');
INSERT INTO `pcr_user` VALUES (35, '90030023', '90030023', 'กระจายสุทธิธรรมโมอนุสรณ์', '90030023', 'sch');
INSERT INTO `pcr_user` VALUES (36, '96022008', '96022008', 'สวนพระยาวิทยา', '96022008', 'sch');
INSERT INTO `pcr_user` VALUES (37, '96022010', '96022010', 'บูกิตประชาอุปถัมภ์', '96022010', 'sch');
INSERT INTO `pcr_user` VALUES (38, '96012004', '96012004', 'บาเจาะ', '96012004', 'sch');
INSERT INTO `pcr_user` VALUES (39, '96012005', '96012005', 'ร่มเกล้า', '96012005', 'sch');
INSERT INTO `pcr_user` VALUES (40, '96012006', '96012006', 'ตันหยงมัส', '96012006', 'sch');
INSERT INTO `pcr_user` VALUES (41, '96012008', '96012008', 'เรียงราษฎ์อุปถัมภ์', '96012008', 'sch');
INSERT INTO `pcr_user` VALUES (42, '96022002', '96022002', 'เวียงสุวรรณวิทยาคม', '96022002', 'sch');
INSERT INTO `pcr_user` VALUES (43, '96012009', '96012009', 'ศรีวารินทร์', '96012009', 'sch');
INSERT INTO `pcr_user` VALUES (44, '96022004', '96022004', 'สุคิรินวิทยา', '96022004', 'sch');
INSERT INTO `pcr_user` VALUES (45, '96022006', '96022006', 'ธัญธารวิทยา', '96022006', 'sch');
INSERT INTO `pcr_user` VALUES (46, '94012008', '94012008', 'วังกะพ้อพิทยาคม', '94012008', 'sch');
INSERT INTO `pcr_user` VALUES (47, '94022006', '94022006', 'ทุ่งยางแดงพิทยาคม', '94022006', 'sch');
INSERT INTO `pcr_user` VALUES (48, '94012004', '94012004', 'วุฒิชัยวิทยา', '94012004', 'sch');
INSERT INTO `pcr_user` VALUES (49, '94022005', '94022005', 'ศิริราษฏร์สามัคคี', '94022005', 'sch');
INSERT INTO `pcr_user` VALUES (50, '94012005', '94012005', 'สายบุรี \"แจ้งประคาร\"', '94012005', 'sch');
INSERT INTO `pcr_user` VALUES (51, '94022009', '94022009', 'แม่ลานวิทยา', '94022009', 'sch');
INSERT INTO `pcr_user` VALUES (52, '94012006', '94012006', 'ไม้แก่นกิตติวิทย์', '94012006', 'sch');
INSERT INTO `pcr_user` VALUES (53, '94022007', '94022007', 'ประตูโพธิ์วิทยา', '94022007', 'sch');
INSERT INTO `pcr_user` VALUES (54, '94022004', '94022004', 'ยาบีบรรณวิทย์', '94022004', 'sch');
INSERT INTO `pcr_user` VALUES (55, '95022006', '95022006', 'กาบังพิทยาคม', '95022006', 'sch');
INSERT INTO `pcr_user` VALUES (56, '95022004', '95022004', 'ธารโตวัฑฒวิทย์', '95022004', 'sch');
INSERT INTO `pcr_user` VALUES (57, '95022003', '95022003', 'บันนังสตาวิทยา', '95022003', 'sch');
INSERT INTO `pcr_user` VALUES (58, '95022002', '95022002', 'จันทร์ประภัสสร์อนุสรณ์', '95022002', 'sch');
INSERT INTO `pcr_user` VALUES (59, '95012002', '95012002', 'รามันห์ศิริวิทย์', '95012002', 'sch');
INSERT INTO `pcr_user` VALUES (60, '95022005', '95022005', 'ยะหาศิรยานุกูล', '95022005', 'sch');
INSERT INTO `pcr_user` VALUES (61, '90032004', '90032004', 'จะนะชนูปถัมภ์', '90032004', 'sch');
INSERT INTO `pcr_user` VALUES (62, '90032005', '90032005', 'ทับช้างวิทยาคม', '90032005', 'sch');
INSERT INTO `pcr_user` VALUES (63, '90032012', '90032012', 'สะบ้าย้อยวิทยา', '90032012', 'sch');
INSERT INTO `pcr_user` VALUES (64, '90032001', '90032001', 'เทพา', '90032001', 'sch');
INSERT INTO `pcr_user` VALUES (65, '9601', '9601', 'นราธิวาส เขต 1', '9601', 'area');
INSERT INTO `pcr_user` VALUES (66, '9602', '9602', 'นราธิวาส เขต 2', '9602', 'area');
INSERT INTO `pcr_user` VALUES (67, '9603', '9603', 'นราธิวาส เขต 3', '9603', 'area');
INSERT INTO `pcr_user` VALUES (68, '9401', '9401', 'ปัตตานี เขต 1', '9401', 'area');
INSERT INTO `pcr_user` VALUES (69, '9402', '9402', 'ปัตตานี เขต 2', '9402', 'area');
INSERT INTO `pcr_user` VALUES (70, '9403', '9403', 'ปัตตานี เขต 3', '9403', 'area');
INSERT INTO `pcr_user` VALUES (71, '9501', '9501', 'ยะลา เขต 1 ', '9501', 'area');
INSERT INTO `pcr_user` VALUES (72, '9502', '9502', 'ยะลา เขต 2 ', '9502', 'area');
INSERT INTO `pcr_user` VALUES (73, '9503', '9503', 'ยะลา เขต 3 ', '9503', 'area');
INSERT INTO `pcr_user` VALUES (74, '9003', '9003', 'สงขลา เขต 3', '9003', 'area');
INSERT INTO `pcr_user` VALUES (75, '101715', '101715', 'สพม. เขต 15', '101715', 'area');
INSERT INTO `pcr_user` VALUES (76, '101716', '101716', 'สพม. เขต 16', '101716', 'area');
INSERT INTO `pcr_user` VALUES (77, 'obec', 'obec', 'สพฐ', NULL, 'obec');

-- ----------------------------
-- View structure for pcr_view_form_school
-- ----------------------------
DROP VIEW IF EXISTS `pcr_view_form_school`;
CREATE ALGORITHM = UNDEFINED DEFINER = `yala1_pracharath`@`%` SQL SECURITY DEFINER VIEW `pcr_view_form_school` AS select `pcr_form`.`id` AS `id`,`pcr_form`.`pid` AS `pid`,`pcr_form`.`title` AS `title`,`pcr_form`.`fname` AS `fname`,`pcr_form`.`lname` AS `lname`,`pcr_form`.`bdate` AS `bdate`,`pcr_form`.`religion` AS `religion`,`pcr_form`.`stud_level` AS `stud_level`,`pcr_form`.`smis` AS `smis`,`pcr_form`.`consider` AS `consider`,`pcr_form`.`file1` AS `file1`,`pcr_form`.`file1_name` AS `file1_name`,`pcr_form`.`file2` AS `file2`,`pcr_form`.`file2_name` AS `file2_name`,`pcr_form`.`form_date` AS `form_date`,`pcr_form`.`create_user_id` AS `create_user_id`,`pcr_form`.`update_user_id` AS `update_user_id`,`pcr_form`.`create_at` AS `create_at`,`pcr_form`.`update_at` AS `update_at`,`pcr_school`.`area_id` AS `area_id`,`pcr_school`.`name` AS `sch_name`,`pcr_school`.`amp_name` AS `amp_name`,`pcr_school`.`prov_id` AS `prov_id` from (`pcr_form` left join `pcr_school` on((`pcr_form`.`smis` = `pcr_school`.`smis`)));

-- ----------------------------
-- View structure for pcr_view_user_school
-- ----------------------------
DROP VIEW IF EXISTS `pcr_view_user_school`;
CREATE ALGORITHM = UNDEFINED DEFINER = `yala1_pracharath`@`%` SQL SECURITY DEFINER VIEW `pcr_view_user_school` AS select `pcr_user`.`id` AS `id`,`pcr_user`.`usr` AS `usr`,`pcr_user`.`pwd` AS `pwd`,`pcr_user`.`name` AS `user_name`,`pcr_user`.`smis` AS `smis`,`pcr_user`.`u_type` AS `u_type`,`pcr_school`.`area_id` AS `area_id`,`pcr_school`.`name` AS `school_name`,`pcr_school`.`amp_name` AS `amp_name`,`pcr_school`.`prov_id` AS `prov_id`,`pcr_school`.`sch_order` AS `sch_order` from (`pcr_user` left join `pcr_school` on((`pcr_user`.`smis` = `pcr_school`.`smis`)));

SET FOREIGN_KEY_CHECKS = 1;
