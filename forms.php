<?php 
session_start();
include("config.php");
include("function.php");
include("head.php");
include("connect.php");

// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

$db->orderBy("form_date","asc");
if($_SESSION['u_type'] == 'area') $db->where("area_id", $_SESSION['smis']);
if($_SESSION['u_type'] == 'sch') $db->where("smis", $_SESSION['smis']);
$schools = $db->get('view_form_school'); //contains an Array of all users 
?>

<section class="content-header">
<h1> ใบสมัคร </h1>
</section> <!-- content-header -->

<section class="content">

<div class="box">
<div class="box-header with-border">
<a class="btn btn-primary" href="form_add.php" role="button">เพิ่มใบสมัคร</a>     
</div>

<div class="box-body">


<table id="example1" class="table table-bordered table-hover">
<thead>
<tr>
  <th>เลขบัตร</th>
  <th>ชื่อ-สกุล</th>
  <th>วันที่สมัคร</th>
  <th>โรงเรียน</th>
  <th>ระดับชั้น</th>
  <th>การพิจารณา</th>
  <th>#</th>
</tr>
</thead>
<tbody>
<?php
foreach($schools as $u){
	$buttonHtml = " <a class='btn btn-xs btn-default' href='form.php?id={$u['id']}' role='button'>รายละเอียด</a>
							<a class='btn btn-xs btn-info' href='form_upload.php?id={$u['id']}' role='button'>อับโหลดไฟล์</a> 
							<a class='btn btn-xs btn-primary' href='form_edit.php?id={$u['id']}' role='button'>แก้ไข</a> 
							<a class='btn btn-xs btn-danger btn-del-confirm' href='form_save.php?del=1&id={$u['id']}' role='button'>ลบ</a>";
	$stud_name = $title_list[$u['title']] . $u['fname'] . ' ' . $u['lname'];
	$form_date = mysql2thaidate($u['form_date']);
	echo "<tr>
			  <td>{$u['pid']}</td>
			  <td>{$stud_name}</td>
			  <td>{$form_date}</td>
			  <td>{$u['sch_name']}</td>
			  <td>{$stud_level_list[$u['stud_level']]}</td>
			  <td>{$consider_list[$u['consider']]}</td>
			  <td>{$buttonHtml}</td>
			</tr>";
}
?>

</table>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->


<script>
  $(function () {
    $('#example1').DataTable({"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]})
  })
</script>

<?php include("foot.php") ?>