<div class="form-group">
  <label for="" class="col-sm-4 control-label">วันที่สมัคร</label>
  <div class="col-sm-2">
	<select class="form-control" name="dd1" id="dd1" required>
	<option >  </option>
	<?php 
	for ($x = 1; $x <= 31; $x++) {
		$dSelected = date("d") == $x ? 'Selected' : '';
		echo "<option value='{$x}' {$dSelected}> {$x} </option>";	
	}
	?>
	</select>
  </div>
  <div class="col-sm-4">
	<select class="form-control" name="mm1" id="mm1" required>
	<option >  </option>
	<?php 
	
	foreach ($month['long'] as $v => $t) {
		$mSelected = str_pad(date("m"), 2, "0", STR_PAD_LEFT) == $v ? 'Selected' : '';
		echo "<option value='{$v}' {$mSelected}> {$t} </option>";	
	}
	?>
	</select>
  </div>
  <div class="col-sm-2">
	<select class="form-control" name="yy1" id=="yy1" required>
	<?php 
	//for ($x = 1998; $x <= 2018; $x++) {
	//	$ySelected = date("Y") == $x ? 'Selected' : 'no';
		$x = 2020;
		echo "<option value='{$x}' > " .($x+543). " </option>";	
	//}
	?>
	</select>
	
  </div>
</div>

<div class="form-group">
  <label for="pid" class="col-sm-4 control-label">เลขบัตรประชาชน</label>
  <div class="col-sm-8">
	<input type="text" class="form-control" id="pid" name="pid" placeholder="เลขบัตรประชาชน" maxlength='13' required>
  </div>
</div>

<div class="form-group">
  <label for="title" class="col-sm-4 control-label">คำนำหน้า</label>
  <div class="col-sm-8">
	<select class="form-control" name="title" id="title" required>
	<option >  </option>
	<?php 
	foreach($title_list as $v => $t){
		echo "<option value='{$v}'> {$t} </option>";	
	}
	?>
	</select>
  </div>
</div>

<div class="form-group">
  <label for="fname" class="col-sm-4 control-label">ชื่อ</label>
  <div class="col-sm-8">
	<input type="text" class="form-control" id="fname" name="fname" placeholder="ชื่อ" required>
  </div>
</div>

<div class="form-group">
  <label for="lname" class="col-sm-4 control-label">นามสกุล</label>
  <div class="col-sm-8">
	<input type="text" class="form-control" id="lname" name="lname" placeholder="นามสกุล" required>
  </div>
</div>

<div class="form-group">
  <label for="" class="col-sm-4 control-label">วันเกิด</label>
  
  <div class="col-sm-2">
	<select class="form-control" name="dd" id="dd" required>
	<option >  </option>
	<?php 
	for ($x = 1; $x <= 31; $x++) {
		echo "<option value='{$x}'> {$x} </option>";	
	}
	?>
	</select>
  </div>
  <div class="col-sm-4">
	<select class="form-control" name="mm" id="mm" required>
	<option >  </option>
	<?php 
	foreach ($month['long'] as $v => $t) {
		echo "<option value='{$v}'> {$t} </option>";	
	}
	?>
	</select>
  </div>
  <div class="col-sm-2">
	<select class="form-control" name="yy" id="yy" required>
	<option >  </option>
	<?php 
	for ($x = 1998; $x <= 2018; $x++) {
		echo "<option value='{$x}'> " .($x+543). " </option>";	
	}
	?>
	</select>
	
  </div>
</div>

<div class="form-group">
  <label for="religion" class="col-sm-4 control-label">ศาสนา</label>
  <div class="col-sm-8">
	<select class="form-control" name="religion" id="religion" required>
	<option >  </option>
	<?php 
	foreach($religion_list as $v => $t){
		echo "<option value='{$v}'> {$t} </option>";	
	}
	?>
	</select>
  </div>
</div>


<div class="form-group">
  <label for="stud_level" class="col-sm-4 control-label">ระดับชั้น</label>
  <div class="col-sm-8">
	<select class="form-control" name="stud_level" id="stud_level" required>
	<option >  </option>
	<?php 
	foreach($stud_level_list as $v => $t){
		echo "<option value='{$v}'> {$t} </option>";	
	}
	?>
	</select>
  </div>
</div>

<div class="form-group">
  <label for="smis" class="col-sm-4 control-label">โรงเรียน</label>
  <div class="col-sm-8">
	<select class="form-control" name="smis" id="smis" required>
	<?php
	$db->orderBy("sch_order","asc");
	if($_SESSION['u_type'] === 'sch')
		$db->where("smis", $_SESSION['smis']);
	else if($_SESSION['u_type'] === 'area')
		$db->where("area_id", $_SESSION['smis']);
	
	$schools = $db->get('school'); //contains an Array of all users 
	
	foreach($schools as $s){
		echo "<option value='{$s['smis']}'> {$s['name']} </option>";	
	}
	?>
	</select>
  </div>
</div>

<div class="form-group">
  <label for="consider" class="col-sm-4 control-label">ข้อมูลการพิจารณา</label>
  <div class="col-sm-8">
	<select class="form-control" name="consider" id="consider" required>
	<option >  </option>
	<?php 
	foreach($consider_list as $v => $t){
		echo "<option value='{$v}'> {$t} </option>";	
	}
	?>
	</select>
  </div>
</div>
