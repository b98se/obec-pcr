<?php 
session_start();
include("config.php"); 
include("function.php");
include("head.php"); 
include("connect.php");

// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

?>

<section class="content-header">
<h1> อับไฟล์ใบสมัคร </h1>
</section> <!-- content-header -->


<section class="content">

<div class="box">
<div class="box-body">

<form class="form" method='post' enctype="multipart/form-data" action='form_save.php'>


<div class="form-group">
  <label for="file1">อับโหลดไฟล์ที่ 1</label>
  <input type="file" id="file1" name="file1" required>
  <p class="help-block">รองรับไฟล์ <?php  foreach ($allowFileType as $a){ echo $a . ', '; } ?> </p>
</div>
<div class="form-group">
  <label for="file1_name">ชื่อไฟล์ที่ 1</label>
  <input type="text" class="form-control" id="file1_name" name="file1_name" required>
</div>

<hr>

<div class="form-group">
  <label for="file1">อับโหลดไฟล์ที่ 2</label>
  <input type="file" id="file2" name="file2" >
  <p class="help-block">รองรับไฟล์ <?php  foreach ($allowFileType as $a){ echo $a . ', '; } ?> </p>
</div>
<div class="form-group">
  <label for="file2_name">ชื่อไฟล์ที่ 2</label>
  <input type="text" class="form-control" id="file2_name" name="file2_name" >
</div>

				
<div class="box-footer text-center">
<input type="hidden" id="upload" name="upload" value="1">
<input type="hidden" id="form_id" name="form_id" value="<?php echo $_GET['id'] ?>">
<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
</div>
</form>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->

<script>
$(function () {
	$("input[name='file1']").change(function() {$("#file1_name").val(getFilename($(this).val()))});
	$("input[name='file2']").change(function() {$("#file2_name").val(getFilename($(this).val()))});
	
});
  function getFilename(fullPath){
	if (fullPath) {
	var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	var filename = fullPath.substring(startIndex);
		if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
			filename = filename.substring(1);
		}
		return filename;
	}
	  
  }
</script>
<?php include("foot.php") ?>