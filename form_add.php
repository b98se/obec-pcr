<?php 
session_start();
include("config.php"); 
include("function.php");
include("head.php"); 
include("connect.php");

// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

?>

<section class="content-header">
<h1> กรอกข้อมูลใบสมัคร </h1>
</section> <!-- content-header -->


<section class="content">

<div class="box">
<div class="box-body">

<form class="form-horizontal" method='post' action='form_save.php'>

<?php include('form_form.php') ?>

<div class="box-footer text-center">
<input type="hidden" id="form_type" name="form_type" value="add">
<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
</div>
</form>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->

<?php include("foot.php") ?>