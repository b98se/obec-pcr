<nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><?php echo $sysTitle ?></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="schools.php">โรงเรียน</a></li>
			
			<?php if(chkAuthorize(array('obec', 'area', 'sch'))):?>
            <li><a href="forms.php">ใบสมัคร</a></li>
			<?php endif; ?>
			
			<?php if(chkAuthorize(array('obec', 'area'))): ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">ผู้ดูแลระบบ <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="users.php">รายชื่อผู้ใช้</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
			<?php endif; ?>
          </ul>

        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="user user-menu">
              <?php if(empty($_SESSION['user_id'])): ?>
				<a href="login.php">เข้าสู่ระบบ</a>
			  <?php else: ?>
				  <a href="user.php">
					<i class="fa fa-fw fa-user"></i>
					<span class="hidden-xs"><?php echo $_SESSION['user_name'] ?></span>
				  </a>
				  
			  <?php endif; ?>
            </li>
			<?php if(!empty($_SESSION['user_id'])): ?>
			<li class="user user-menu">
				<a href="login.php?logout=1"><i class="fa fa-fw fa-sign-out"></i>
					<span class="hidden-xs">ออก</span>
				</a>
			</li>
			<?php endif; ?>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>