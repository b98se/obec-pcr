<?php 
session_start();
include("config.php"); 
include("function.php");
include("head.php"); 
include("connect.php");
// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

$db->where ("id", $_GET['id']);
$form = $db->getOne ("form");
//print_r($form);
?>

<section class="content-header">
<h1> ข้อมูลใบสมัคร </h1>
</section> <!-- content-header -->


<section class="content">

<div class="box">
<div class="box-body">

<b>ข้อมูลนักเรียน</b>
<dl class="dl-horizontal">
	<dt>เลขบัตรประชาชน</dt>
	<dd><?php echo $form['pid']; ?></dd>

	<dt>ชื่อ-สกุล</dt>
	<dd><?php echo $title_list[$form['title']] . $form['fname'] . ' ' . $form['lname']; ?></dd>

	<dt>วันเดือนปี เกิด</dt>
	<dd><?php echo mysql2thaidate($form['bdate']); ?></dd>
	<dt>ศาสนา</dt>
	<dd><?php echo $religion_list[$form['religion']]; ?></dd>
</dl>

<b>ข้อมูลการสมัคร</b>
<dl class="dl-horizontal">
	<dt>วันที่สมัคร</dt>
	<dd><?php echo mysql2thaidate($form['form_date']); ?></dd>

	<dt>ระดับชั้น</dt>
	<dd><?php echo $stud_level_list[$form['stud_level']]; ?></dd>
	
	<dt>โรงเรียน</dt>
	<dd>
	<?php 
	$db->where ("smis", $form['smis']);
	$sch = $db->getOne ("school");
	echo $sch['name']; 
	?>
	</dd>
	<dt>ข้อมูลการพิจารณา</dt>
	<dd><?php echo $consider_list[$form['consider']]; ?></dd>
</dl>

<b>ไฟล์หลักฐาน</b>
<dl class="dl-horizontal">
<?php 
if(!empty($form['file1'])){
	$file1name = empty($form['file1_name']) ? 'ไฟล์ 1' : $form['file1_name'];
	$file1url = $target_dir . $form['file1'];
	echo " <dt>ไฟล์ที่ 1</dt>  <dd><a href='{$file1url}' target=blank>{$file1name}</a></dd>";
}	
if(!empty($form['file2'])){
	$file2name = empty($form['file2_name']) ? 'ไฟล์ 2' : $form['file2_name'];
	$file2url = $target_dir . $form['file2'];
	echo " <dt>ไฟล์ที่ 2</dt>  <dd><a href='{$file2url}' target=blank>{$file2name}</a></dd>";
}	
	
?>
	
</dl>


<div class="box-footer text-center">

<a class='btn btn-primary' href='form_edit.php?id=<?php echo $form['id']?>' role='button'>แก้ไข</a> 
</div>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->

<script>
    var js_data = '<?php echo json_encode($form); ?>';
    var js_obj_data = JSON.parse(js_data );
    var fieldexclude = [<?php //echo $fieldexclude ?>];
    console.log(js_data);

    $.each( js_obj_data, function( key, value ) {
		
		if(key == 'bdate'){
			//console.log('key=' + key + '   value=' + value);
			var bdate = value.split("-");
			$("#dd" ).val(parseInt(bdate[2]));
			$("#mm" ).val(bdate[1]);
			$("#yy" ).val(bdate[0]);
		}
		if(key == 'form_date'){
			//console.log('key=' + key + '   value=' + value);
			var fdate = value.split("-");
			$("#dd1" ).val(parseInt(fdate[2]));
			$("#mm1" ).val(fdate[1]);
			$("#yy1" ).val(fdate[0]);
		}

        //console.log('fieldexclude.indexOf(key) = ' + fieldexclude.indexOf(key) + ' key =' + key);
        if(fieldexclude.indexOf(key) == '-1'){
            $("#" + key).val(value);
            console.log('key=' + key + '   value=' + value);
        }

    });

  $(function () {
    
  })
</script>

<?php include("foot.php") ?>