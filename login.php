<?php 
include("config.php"); 
include("function.php"); 

if($_GET['logout'] == 1){
	session_start();
	session_destroy(); 
	$_SESSION = [];
	goUrl('index.php');
	die();
}

if(!empty($_POST['usr'])){
	include("connect.php"); 
	
	$db->where("usr", $_POST['usr']);
	$db->where("pwd", $_POST['pwd']);
	$user = $db->getOne("user");
	if ($db->count > 0){
		session_start();
		$_SESSION['user_id'] = $user['id'];
		$_SESSION['user_name'] = $user['name'];
		$_SESSION['smis'] = $user['smis'];
		$_SESSION['u_type'] = $user['u_type'];
		goUrl('index.php');
	}else{
		jsAlert("ชื่อผู้ใช้ และ รหัสผ่าน ไม่ถูกต้อง");
		goUrl('login.php');
	}
	
	die();
}

include("head.php"); 
?>

<div class="login-box">

  <div class="login-box-body">
    <p class="login-box-msg">เข้าสู่ระบบ</p>

    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" name="usr" class="form-control" placeholder="ชื่อผู้ใช้" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="pwd" class="form-control" placeholder="รหัสผ่าน" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">

        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">เข้าสู่ระบบ</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include("foot.php") ?>