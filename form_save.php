<?php
session_start();
include("config.php"); 
include("function.php"); 
include("connect.php"); 
// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

if(!empty($_POST['form_type'])){
	$bdate = date_create_from_format('Ymd', $_POST['yy'].$_POST['mm'].$_POST['dd']);
	$_POST['bdate'] = date_format($bdate, 'Y-m-d');
	
	$fdate = date_create_from_format('Ymd', $_POST['yy1'].$_POST['mm1'].$_POST['dd1']);
	$_POST['form_date'] = date_format($fdate, 'Y-m-d');
	
	unset($_POST['yy'], $_POST['mm'], $_POST['dd'], $_POST['yy1'], $_POST['mm1'], $_POST['dd1']);
}

if($_POST['form_type'] == 'add'){
	unset($_POST['form_type']);
	$_POST['create_user_id'] = $_SESSION['user_id'];
	$_POST['create_at'] = date('Y-m-d H:i:s');
	//print_r($_POST);
		
	$db->insert ('form', $_POST);
	if($db->getLastErrno() === 0){
		jsAlert("บันทึกข้อมูลเรียบร้อยแล้ว");
		goUrl('forms.php');
	}else{
		jsAlert('เกิดข้อผิดพลาดในการบันทึกข้อมูล. Error: ' . $db->getLastError());
		goBack();
	}
	die();
}

if($_POST['form_type'] == 'edit'){
	$_POST['update_user_id'] = $_SESSION['user_id'];
	$_POST['update_at'] = date('Y-m-d H:i:s');
	$id = $_POST['form_id'];
	unset($_POST['form_type'], $_POST['form_id']);

	$db->where ('id', $id);
	if ($db->update ('form', $_POST)){
		jsAlert("บันทึกข้อมูลเรียบร้อยแล้ว");
		goUrl('forms.php');
	}else{
		jsAlert('เกิดข้อผิดพลาดในการบันทึกข้อมูล. Error: ' . $db->getLastError());
		goBack();
	}
	die();
}

if($_POST['upload'] == '1'){
	$db->where ('id', $_POST['form_id']);
	
	$data['update_user_id'] = $_SESSION['user_id'];
	$data['update_at'] = date('Y-m-d H:i:s');
	
	$data['file1_name'] = $_POST['file1_name'];
	
	$r = fileUpload($_FILES['file1'], ($_POST['form_id'].'_1'));
	if($r['status'] == true){
		$data['file1'] = $r['fileUploaded'];

		$db->update ('form', $data);
	}else{
		jsAlert('ไฟล์ที่ 1 ' . $r['error']);
		goBack();
	}
	unset($data['file1'], $data['file1_name']);
	
	if(!empty($_FILES['file2'])){
		
		$data['file2_name'] = $_POST['file2_name'];
		
		$r2 = fileUpload($_FILES['file2'], ($_POST['form_id'].'_2'));
		if($r2['status'] == true){
			$data['file2'] = $r2['fileUploaded'];

			$db->update ('form', $data);
		}else{
			jsAlert('ไฟล์ที่ 2 ' . $r2['error']);
			goBack();
		}
	}
	jsAlert("บันทึกข้อมูลเรียบร้อยแล้ว");
	goUrl('forms.php');
	
	die();
}

if($_GET['del'] == 1){
	//print_r($_GET);
	$db->where('id', $_GET['id']);
	if($db->delete('form')) {
		jsAlert("ลบข้อมูลเรียบร้อยแล้ว");
		goUrl('forms.php');
	}else{
		jsAlert('เกิดข้อผิดพลาดในการลบข้อมูล. Error: ' . $db->getLastError());
		goBack();
	}
	die();
}
/*
`pid` varchar(13) NOT NULL,
`title` varchar(2) DEFAULT NULL,
`fname` varchar(255) DEFAULT NULL,
`lname` varchar(255) DEFAULT NULL,
`bdate` date DEFAULT NULL,
`religion` varchar(10) DEFAULT NULL,
`stud_level` varchar(2) DEFAULT NULL,
`smis` varchar(8) NOT NULL,
`consider` varchar(2) DEFAULT NULL,
`form_date` date DEFAULT NULL,
`create_at` datetime DEFAULT NULL,
*/
?>