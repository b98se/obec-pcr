<?php 
session_start();
include("config.php"); 
include("function.php");
include("head.php"); 
include("connect.php");
// เช็คสิทธิ์ $_SESSION['u_type']
if(!authorize(array('obec', 'area', 'sch'))) die();

$db->where ("id", $_GET['id']);
$form = $db->getOne ("form");
//print_r($form);
?>

<section class="content-header">
<h1> แก้ไขข้อมูลใบสมัคร </h1>
</section> <!-- content-header -->


<section class="content">

<div class="box">
<div class="box-body">

<form class="form-horizontal" method='post' action='form_save.php'>

<?php include('form_form.php') ?>

<div class="box-footer text-center">

<input type="hidden" id="form_type" name="form_type" value="edit">
<input type="hidden" id="form_id" name="form_id" value="<?php echo $form['id'] ?>">
<button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
</div>
</form>

</div> <!-- /.box-body -->
</div><!-- /.box -->

</section><!--  content -->

<script>
    var js_data = '<?php echo json_encode($form); ?>';
    var js_obj_data = JSON.parse(js_data );
    var fieldexclude = [<?php //echo $fieldexclude ?>];
    console.log(js_data);

    $.each( js_obj_data, function( key, value ) {
		
		if(key == 'bdate'){
			//console.log('key=' + key + '   value=' + value);
			var bdate = value.split("-");
			$("#dd" ).val(parseInt(bdate[2]));
			$("#mm" ).val(bdate[1]);
			$("#yy" ).val(bdate[0]);
		}
		if(key == 'form_date'){
			//console.log('key=' + key + '   value=' + value);
			var fdate = value.split("-");
			$("#dd1" ).val(parseInt(fdate[2]));
			$("#mm1" ).val(fdate[1]);
			$("#yy1" ).val(fdate[0]);
		}

        //console.log('fieldexclude.indexOf(key) = ' + fieldexclude.indexOf(key) + ' key =' + key);
        if(fieldexclude.indexOf(key) == '-1'){
            $("#" + key).val(value);
            console.log('key=' + key + '   value=' + value);
        }

    });

  $(function () {
    
  })
</script>

<?php include("foot.php") ?>